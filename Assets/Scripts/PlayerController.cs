﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public bool onIce = false;
    public float speed;
    private Vector2 movement;

    Rigidbody2D rigidbody2d;
    Vector2 lookDirection = new Vector2(1, 0);

    // Start is called before the first frame update
    void Start()
    {
        rigidbody2d = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        movement.x = Input.GetAxisRaw("Horizontal");
        movement.y = Input.GetAxisRaw("Vertical");

        if (!Mathf.Approximately(movement.x, 0.0f) || !Mathf.Approximately(movement.y, 0.0f))
        {
            lookDirection.Set(movement.x, movement.y);
            lookDirection.Normalize();
        }

        if (Input.GetKeyDown(KeyCode.E))
        {
            RaycastHit2D hit = Physics2D.Raycast(rigidbody2d.position + Vector2.up * 0.2f, lookDirection, 1.5f, LayerMask.GetMask("Objetos"));
            if (hit.collider != null)
            {
                Debug.Log("Interaccion realizada");
            }
        }
    }

    void FixedUpdate()
    {
        if (!onIce)
        {
            rigidbody2d.MovePosition(rigidbody2d.position + movement * speed * Time.deltaTime);
        }
        else
        {
            rigidbody2d.AddForce(movement * speed);
        }
    }
}
